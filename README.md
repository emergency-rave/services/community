# EmRAVE community

This project contains a snowflake docker image for the staging community app.

## prepare log :de:



### Host (DB-Server) aus Sicht des OpenSocial Containers ermitteln (= Gateway)


```bash
PS C:\\Users\\hhaus\> docker ps

CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS NAMES

b5af486dec35 mysql/mysql-server:latest \"/entrypoint.sh mysq...\" 33
minutes ago Up 33 minutes (healthy) 0.0.0.0:3306-\>3306/tcp, 33060/tcp
mysql01

1bc3161b87f1 goalgorilla/open\_social\_docker \"docker-php-entrypoi...\"
2 weeks ago Up 38 minutes 0.0.0.0:80-\>80/tcp objective\_yalow

PS C:\\Users\\hhaus\> docker inspect objective\_yalow

\[

{

\"Id\":
\"1bc3161b87f16e20ee41454759cdf52f9ceaf637b6ff10b819e0d4ff0ecb7598\",

\"Created\": \"2020-06-07T01:53:05.0233833Z\",
...

\"Gateway\": \"172.17.0.1\",

...



\]

```




OpenSocial Container im UI konfigurieren
----------------------------------------

![](media/image1.png){width="6.3in" height="3.8243055555555556in"}

Fehlermeldung bei "Install Open Social Modules":

```
Path:
/core/install.php?rewrite=ok&profile=social&langcode=en&id=2&op=do\_nojs&op=do

StatusText: OK

ResponseText: Drupal\\Core\\Entity\\EntityStorageException: Exception
thrown while performing a schema update. SQLSTATE\[42000\]: Syntax error
or access violation: 1064 You have an error in your SQL syntax; check
the manual that corresponds to your MySQL server version for the right
syntax to use near \'groups (

\`id\` INT unsigned NOT NULL auto\_increment,

\`type\` VARCHAR(32) CHARACTE\' at line 1: CREATE TABLE {groups} (

\`id\` INT unsigned NOT NULL auto\_increment,

\`type\` VARCHAR(32) CHARACTER SET ascii COLLATE ascii\_general\_ci NOT
NULL COMMENT \'The ID of the target entity.\',

\`uuid\` VARCHAR(128) CHARACTER SET ascii COLLATE ascii\_general\_ci NOT
NULL,

\`langcode\` VARCHAR(12) CHARACTER SET ascii COLLATE ascii\_general\_ci
NOT NULL,

PRIMARY KEY (\`id\`),

UNIQUE KEY \`group\_field\_\_uuid\_\_value\` (\`uuid\`),

INDEX \`group\_field\_\_type\_\_target\_id\` (\`type\`)

) ENGINE = InnoDB DEFAULT CHARACTER SET utf8mb4 COMMENT \'The base table
for group entities.\'; Array

(

)

in
Drupal\\Core\\Entity\\Sql\\SqlContentEntityStorage-\>wrapSchemaException()
(line 1491 of
/var/www/html/core/lib/Drupal/Core/Entity/Sql/SqlContentEntityStorage.php).

```

Commit images and push to registry
----------------------------------

```bash
PS C:\\Users\\hhaus\> docker ps

CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS NAMES

b5af486dec35 mysql/mysql-server:latest \"/entrypoint.sh mysq...\" 2
hours ago Up 2 hours (healthy) 0.0.0.0:3306-\>3306/tcp, 33060/tcp
mysql01

1bc3161b87f1 goalgorilla/open\_social\_docker \"docker-php-entrypoi...\"
2 weeks ago Up 2 hours 0.0.0.0:80-\>80/tcp objective\_yalow


PS C:\\Users\\hhaus\> docker login registry.gitlab.com

Username: h.muc.hauschild\@googlemail.com

Password:

Login Succeeded

PS C:\\Users\\hhaus\> docker commit objective\_yalow
registry.gitlab.com/emergency-rave/services/community

sha256:da861b77cc12b6834de556f21285ba3e8f9d3510077c6a2b41448324a09ec755

PS C:\\Users\\hhaus\> docker push
registry.gitlab.com/emergency-rave/services/community

The push refers to repository
\[registry.gitlab.com/emergency-rave/services/community\]

a8e1d86570b8: Pushed

2292e6d386a3: Pushed

267d92c59b0d: Pushed

21f4eb40ca74: Pushed

39209654794f: Pushed

cdf7e1241bb7: Pushed

4d9b5e5caafc: Pushed

9a9d330d3d30: Pushed

0dc7c5ff40d8: Pushed

cfa4569d8d01: Pushed

d3d1c08a3874: Pushed

0fb663ed70f7: Pushed

5cdb8a459019: Pushed

6db196e52d33: Pushed

c821371f9602: Pushed

172583a85aa8: Pushed

23d2e15056c0: Pushed

9501b14b2180: Pushed

715f42180fe9: Pushed

47154fb7722b: Pushed

674c8c1065eb: Pushed

980011709307: Pushed

ab6b916210f7: Pushed

9505b0d91bd3: Pushed

511a759fb701: Pushed

895802b60b42: Pushed

061fa9f2c26d: Pushed

fe0d1f11d16f: Pushed

cd713262a02b: Pushed

d42da8e0c3e4: Pushed

6270adb5794c: Pushed

latest: digest:
sha256:bea8d68218972c08ab86fd503610dfe6e5d017af45d83ba555a6ba2fd5646e60
size: 6800



```